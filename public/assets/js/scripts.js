$(window).bind("pageshow", function(event) {
    $('.zoom-box').removeClass('zoom');
});
$(document).ready(function() {

		//Animate Logo
		$('#bc_logo').animate({opacity: 1}, 500);
		$('#flame, .creative-c, .cls-1, .cls-2').attr('transform', 'translate(-500, -7.2)');
		$('#bc_logo').hoverIntent(
			function() {
				$(this).parent().addClass('hovered');

				var maxLoops = -17;
				var i = -500;
				var  o = 0;
				(function next() {
				    if (i++ >= maxLoops) { return; }

				    setTimeout(function() {
				       $('#flame, .creative-c, .cls-1, .cls-2').attr('transform', 'translate(' + i + ', -7.2)');
						$('.hide').css('opacity', o);
						i = i +9;
				       o = o + 0.02070393375;
				        next();
				    }, 1);
				})();


 			},
			function() {
				$(this).parent().removeClass('hovered');
				setTimeout( function() {
					var maxLoops = -500;
					var i = -17;
					var  o = 0.99984500;
					(function next() {
					    if (i-- <= maxLoops) { return; }

					    setTimeout(function() {
					       $('.slide').attr('transform', 'translate(' + i + ', -7.2)');
					       $('.hide').css('opacity', o);
					       i = i -9;
					       o = o - 0.02070393375;
					        next();
					    }, 1);
					})();
				}, 50 );

			} //Close Hover function #2
		); //Close Hover

		$('.menu-btn').click( function() {
			$('.menu-btn span').toggleClass('active');
			$('.menu').slideToggle();

		});

		/* Open the Homepage boxes into a new page*/
		$('.box .box-link').click( function(e) {
			e.preventDefault();
			$(this).find('.zoom-box').addClass('zoom');
			var url = $(this).attr("href");

      // setTimeout(function() {
      //     $('.zoom-box').removeClass('zoom');
      //  }, 295);

		   setTimeout(function() {
          
		      window.location = url;
		   }, 300);
		});

		$('.top-menu a').click( function(e) {
			e.preventDefault();
			$(this).find('.zoom-box').addClass('zoom');
			var url = $(this).attr("href");

		   setTimeout(function() {
		      window.location = url;

		   }, 500);
		});

		$('.box').hoverIntent(
			function() {
				$(this).find('.box-slide-down').addClass('active');
			},
			function() {
				$(this).find('.box-slide-down').removeClass('active');
		});

		$('.animateHover').hoverIntent(
			function() {
				$(this).children().addClass('active');
			},
			function() {
				$(this).children().removeClass('active');
		});



		/* Case study Pages*/




	}); //Close Doc Ready


var $window = $(window);

function check_if_in_view() {
  var $animation_elements = $('.animateWrapper');
  var window_top_position = $window.scrollTop();
  var window_height = $(window).height();
  var defualt_animate_time = 3;

  $.each($animation_elements, function() {
    var $element = $(this);
    var animateTop = $element.data('animate-time') || defualt_animate_time;
    var element_top_position = $element.offset().top;
    //check to see if this current container is within viewport
    if (window_top_position >= element_top_position -  ( window_height / (50 / animateTop) ) ) {
      $element.children().addClass('active');
    }
    // else {
    //   $element.removeClass('in-view');
    // }
  });
}

$window.on('scroll resize', check_if_in_view);

$(document).ready(function() {
/*Team Page*/


  // Function for handling hover
  $('.member-picture-wrapper').hoverIntent(
    function() {
      if($(this).find('.click_picture').is(':visible')){
        $(this).find('.member_overlay').removeClass('active');
      }
      else{
        $(this).find('.primary_picture').addClass('hidden');
        $(this).find('.hover_picture').addClass('active');
        $(this).find('.member_overlay').addClass('active');
      }
    },
    function() {
      $(this).find('.primary_picture').removeClass('hidden');
      $(this).find('.hover_picture').removeClass('active');
      $(this).find('.member_overlay').removeClass('active');
    }
  );

  // Function for click to open
  $('.member-picture-wrapper').click(function(e) {
    e.preventDefault();
    if($(this).find('.click_picture').is(':visible')){
      $(this).parent().find('.member_info').removeClass('active');
      $(this).find('.click_picture').removeClass('active');

    }
    else{
      $(this).parent().find('.member_info').addClass('active');
      $(this).find('.click_picture').addClass('active');

    }
  });

  //  if($('body').hasClass('services-page')){
  //   var servicesController = new ScrollMagic.Controller({
  //     globalSceneOptions: {
  //       triggerHook: 'onLeave'
  //     }
  //   });

  //   // get all slides
  //   var slides = document.querySelectorAll("section.panel");

  //   // create scene for every slide
  //   for (var i=0; i<slides.length; i++) {
  //     new ScrollMagic.Scene({
  //         triggerElement: slides[i]
  //       })
  //       .setPin(slides[i])
  //       // .addIndicators() // add indicators (requires plugin)
  //       .addTo(servicesController);
  //   }
  // }
  // Background Scroll
  var wHeight = $(window).innerHeight();
  var siblings = $('.panel').siblings();
  var perset = {};
  var sumHeight = 0;
  for(var i = 0; i<siblings.length; i++) {
    if(siblings[i].dataset.background){
      perset[sumHeight] =  siblings[i].dataset.background;
    }
    else
      perset[sumHeight] =  0;

    sumHeight= sumHeight + siblings[i].clientHeight;
  }
  processScroll();

  function lessThan(nums, key){
    if(nums == null || nums.length == 0 || key ==0 )
      return 0;
    low = 0;
    high = nums.length -1;
    while(low <= high){
        mid = parseInt((low + high) >> 1);
        if(key <= nums[mid]){
            high = mid - 1;
        }else {
            low = mid +1;
        }
    }
    return high;
  }

  var scroll_pos = 0;

  function processScroll() {
    scroll_pos = $(this).scrollTop();

    var presetHeights = Object.keys(perset);
    var x = lessThan(presetHeights,scroll_pos);
    var bgColor = perset[presetHeights[x]];
    if(bgColor) {
      $("body").css('background-color',bgColor);
    }
  }

  $(document).scroll(processScroll);



});