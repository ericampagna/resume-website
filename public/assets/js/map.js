 var map;
 var myLatLng;
 function initMap() {
  myLatLng = new google.maps.LatLng(38.195490,-87.556084);
  map = new google.maps.Map(document.getElementById('map'), {
    center: myLatLng,
    zoom: 9,
    scrollwheel: false,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    draggable: false,

    styles: [
      {
        "stylers": [
          { "saturation": -100 }
        ]
      },{
        "featureType": "water",
        "stylers": [
          { "hue": "#0091ff" },
          { "saturation": 100 }
        ]
      },{
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry",
        "stylers": [
          { "saturation": 100 },
          { "color": "#002F3B" },
          { "weight": 0.7 },
          { "visibility": "simplified" }
        ]
      },{
        "featureType": "landscape",
        "stylers": [
          { "hue": "#ff0000" },
          { "lightness": 100 },
          { "saturation": 86 }
        ]
      }
    ]
  });

  

 

  //google.maps.event.addDomListener(window, 'load', initMap);
 var infowindow = null;
  function addMarker(location) {
      var marker = new google.maps.Marker({
      position: location.position,
      map: map
    });

    
  
    marker.addListener('click', function() {
      if(infowindow){
          infowindow.close();
      }
    
    infowindow = new google.maps.InfoWindow({
    content: location.content
    });
    infowindow.open(map, marker);
  });
  }
  var locations = [ 
    {
      position: new google.maps.LatLng(38.408561, -87.760577),
      content: '<div class="Marker--content">'+
      '<h4>Brushfire Creative</h4>'+
      '<div class="Marker--body">'+
      '<h5>Main Office</h5>'+
      '<p>306 N Market St. <br> Suite 200 <br> Mount Carmel, IL 62863</p>'+
      '<p><a href="tel:16182634547">618-263-4547</a></p>'+
      '</div>'+
      '</div>'
    },
    {
      position: new google.maps.LatLng(37.969325,-87.491016),
      content: '<div class="Marker--content">'+
      '<h4>Brushfire Creative</h4>'+
      '<div class="Marker--body">'+
      '<h5>Evansville Office</h5>'+
      '<p>4915 Lincoln Ave <br> Suite 200 <br> Evansville, IN 47715</p>'+
      '<p><a href="tel:16182634547">618-263-4547</a></p>'+
      '</div>'+
      '</div>'
    }
  ];
  for (var i = 0, location; location = locations[i]; i++) {
    addMarker(location);
  }

 

}

// CustomMarker.prototype = new google.maps.OverlayView();

// CustomMarker.prototype.draw = function() {

//   var self = this;

//   var div = this.div;

//   if (!div) {

//     div = this.div = document.createElement('div');

//     div.className = 'marker';

//     div.style.position = 'absolute';
//     div.style.cursor = 'pointer';
//     div.style.width = '20px';
//     div.style.height = '20px';
//     div.style.background = 'blue';

//     if (typeof(self.args.marker_id) !== 'undefined') {
//       div.dataset.marker_id = self.args.marker_id;
//     }

//     google.maps.event.addDomListener(div, "click", function(event) {
//       alert('You clicked on a custom marker!');
//       google.maps.event.trigger(self, "click");
//     });

//     var panes = this.getPanes();
//     panes.overlayImage.appendChild(div);
//   }

//   var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

//   if (point) {
//     div.style.left = (point.x - 10) + 'px';
//     div.style.top = (point.y - 20) + 'px';
//   }
// };

// CustomMarker.prototype.remove = function() {
//   if (this.div) {
//     this.div.parentNode.removeChild(this.div);
//     this.div = null;
//   }
// };

// CustomMarker.prototype.getPosition = function() {
//   return this.latlng;
// };


