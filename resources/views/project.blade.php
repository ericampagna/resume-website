@extends('layouts.main')

@section('title', 'Start a Project')

@section('description', 'Have an idea for a new project? Need help with an exisiting project?')

@section('keywords', 'Project, Contact for a Website, Marketing Services, Website Project, Business Project')

@section('bodyClass', 'project')

@section('content')
<section class="page-top">
	<h1 class="animated fadeInUp" style="color: #ffffff">Start a Project<small>Have an idea for a new project? Need help with an exisiting project?</small></h1>
</section>

<div class="contact-form">
        <p><i class="fa fa-phone"></i> (618) 878-9188</p>
        <p></i><i class="fa fa-envelope "></i> ericampagna@gmail.com</p>
        {{-- <form class="project-form" role="form" method="POST" action="{{ url('/start-a-project') }}">
         <input id="name" type="text" class="input-full" name="name" value="{{ old('name') }}" placeholder="Name">
         @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
        <input id="company" type="text" class="input-full" name="company" value="{{ old('company') }}" placeholder="Company">
         @if ($errors->has('company'))
            <span class="help-block">
                <strong>{{ $errors->first('company') }}</strong>
            </span>
        @endif
        <input id="email" type="email" class="input-full" name="email" value="{{ old('email') }}" placeholder="E-Mail">

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif

        <input id="phone" type="text" class="input-full" name="phone" value="{{ old('name') }}" placeholder="Phone">
         @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif

        <textarea name="message" cols="50" rows="10" placeholder="How can we help you?"></textarea>
        @if ($errors->has('message'))
            <span class="help-block">
                <strong>{{ $errors->first('message') }}</strong>
            </span>
        @endif

        <div>
            <button type="submit" class="btn submit-button">
                Send
            </button>
        </div>
    </form> --}}
</div>
@endsection