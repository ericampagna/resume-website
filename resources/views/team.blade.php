@extends('layouts.main')

@section('title', 'About the Brushfire Team')

@section('description', 'Find out where we have been and where we are going.')

@section('keywords', 'Brushfire team, design, marketing, Brushfire, Developer, Creative Director, Designer, Senior Developer')

@section('bodyClass', 'team')

@section('content')
<section class="page-top">
	<h1 class="animated fadeInUp" style="color: #ffffff">Small Team, Big Impact<small>
	We are small team of designers, developers and stratagists.</small></h1>
	</section>
	{{-- <section class="team_blocks clearfix">
	 <div class="team-member-row">
	 	<div class="member-picture-wrapper">
	 		<div class="member_overlay">
	 			<p>Meet <br> Brittany Campagna</p>
	 		</div>
	 		<img class="hover_picture" src="/assets/images/Brittany_personality.jpg" alt="Brittany Likes Markers" />
	 		<img class="primary_picture " src="/assets/images/Brittany_profile1.jpeg" alt="Brittany Campagna" />
	 		<img class="click_picture" src="/assets/images/Brittany_headshot.jpg" alt="Brittany Campagna - Owner" />
	 	</div>
	 	<div class="member_info">
	 		<div class="main_block_info">
	 			<h3 class="member_name">Brittany Campagna
	 			<small class="member_position">Managing Partner</small></h3>
	 			<hr>
	 			<p class="member_about">I can't live without: Dark Chocolate</p>
				<p>My weirdness: I run away at the sound of people chewing or hot water being poured into a glass.</p>
	 		</div>
	 		<h4 class="member_twitter" >@BrittAnneC</h4>
	 	</div>
	 </div>
	 <div class="team-member-row">
	 	<div class="member-picture-wrapper">
	 		<div class="member_overlay">
	 			<p>Meet <br> Eric Campagna</p>
	 		</div>
	 		<img class="hover_picture" src="/assets/images/Eric_personality.jpg" alt="Eric Likes Chickens" />
	 		<img class="primary_picture " src="/assets/images/Eric_profile.jpg" alt="Eric Campagna" />
	 		<img class="click_picture" src="/assets/images/Eric_headshot.jpg" alt="Eric Campagna - Owner" />
	 	</div>
	 	<div class="member_info">
	 		<div class="main_block_info">
	 			<h3 class="member_name">Eric Campagna
	 			<small>Partner</small></h3>
	 			<hr>
	 			<p>I can't live without: Coffee</p>
				<p>My weirdness: I'm old-school. I like hand tools, pour-over coffee, real books, pens and pencils. Also I have chickens.</p>
	 		</div>
	 		<h4 class="member_twitter" >@ericampagna</h4>
	 	</div>
	 </div>
	 <div class="team-member-row">
	 	<div class="member-picture-wrapper">
	 		<div class="member_overlay">
	 			<p>Meet <br> Terry Beckerman</p>
	 		</div>
	 		<img class="hover_picture" src="/assets/images/Terry_personality.jpg" alt="Terry Likes Duct Tape" />
	 		<img class="primary_picture " src="/assets/images/Terry_profile-1.jpg" alt="Terry Beckerman" />
	 		<img class="click_picture" src="/assets/images/Terry_headshot.jpg" alt="Terry Beckerman - Partner" />
	 	</div>
	 	<div class="member_info">
	 		<div class="main_block_info">
	 			<h3 class="member_name">Terry Beckerman
	 			<small>Partner</small></h3>
	 			<hr>
	 			<p>I can't live without: Christmas Music</p>
				<p>My weirdness: I put sugar on tomatoes, and always chew loudly.</p>
	 		</div>
	 		<h4 class="member_twitter" >@maplehill82</h4>
	 	</div>
	 </div>
	
	 </div>
</section> --}}
@endsection