@extends('layouts.main')

@section('title', 'Strategy & Design Services')

@section('description', 'We focus on what we are the best at so you can focus on what you are best at.')

@section('keywords', 'marketing, digital marketing, web development, web design, logo design, logo creation, social media management, social media marketing, Brushfire services, marketing services, business cards, brochures, letterhead, banners, t shirt designs, branding, marketing strategy, billboard design, product photography, Identity Design, business naming, platform deisgn, Stationary, style guides, API Integration, Business Portal, Content Managment System, CMS, Dashboard Development, E-commerce, Responsive Web Design, Social Media, Search Engine, Optimazation, SEO, Search Engine Marketing, SEM, Web Applications, Managed Web Presence, custom website')

@section('bodyClass', 'services')

@section('content')
<section class="panel page-top animateWrapper" data-background="rgb(24,182,240)" data-animate-time="-5">
	<h1 class="animated fadeInUp" style="color: #ffffff">Our Services<small>We focus on what we are best at so you can focus on what you are best at.</small></h1>
	<a href="#start"><span class="fa fa-angle-double-down"></span></a>
</section>
<section class="panel animateWrapper" data-background="rgb(28,148,90)">
		<div class="heading animateWrapper" data-animate-time="8">
			<hr class="first" id="start">
			<h1>Print Design</h1>
			<hr class="last">
		</div>
		<div class="content animateWrapper" data-animate-time="6">
			<p>Your business has an identity, and that identity stems from your logo. Once it is in place, there are a million next steps - business cards, brochures, vehicle wraps, retractable banners, promotional items. Your entire branding strategy revolves around your logo. Whatever it is you are looking for, we’re ready to help you take that next step!
			</p>
			
		</div>
		<div class="list">
			<ul class="list-col">
				<li>Banners</li>
				<li>Billboards</li>
				<li>Branding</li>
				<li>Identity Design</li>
				</ul>
			<ul class="list-col">
				<li>Brochures<li>
				<li>Business Cards</li>
				<li>Logo Creation</li>
				<li>Naming</li>
			</ul>
			<ul class="list-col">
				<li>Platform Design</li>
				<li>Stationary</li>
				<li>Style Guides</li>
			</ul>
		</div>
	<span class="fa fa-angle-double-down"></span>
</section>
<section class="panel web animateWrapper" data-background="rgb(213,137,40)">
		<div class="heading animateWrapper" data-animate-time="8">
			<hr class="first">
			<h1>Web Strategy</h1>
			<hr class="last">
		</div>
		<div class="content animateWrapper" data-animate-time="6">
			<p>We don’t just mean a website, web strategy is all about your company’s online image from Facebook and Twitter to online publications and search engine ads. Because so much of our lives happen online, it is crucial for your business to have a game plan on how to get and retain customers through the web. Don’t know where to begin? Come chat with us and we’ll help you get started!</p>
		</div>
		<div class="list">
			<ul class="list-col">
				<li>API Integration</li>
				<li>Business Portal</li>
				<li>Content Managment Systems (CMS)</li>
			</ul>
			<ul class="list-col">
				<li>Dashboard Development</li>
				<li>E-commerce<li>
				<li>Responsive Web Design</li>
				<li>Social Media</li>
			</ul>
			<ul class="list-col">
				<li>Search Engine Optimazation (SEO)</li>
				<li>Search Engine Marketing (SEM)</li>
				<li>Web Applications</li>
			</ul>
		</div>
	<span class="fa fa-angle-double-down"></span>
</section>
<section class="panel spark-services animateWrapper" data-background="rgb(213,83,40)">
		<div class="col-xs-12 col-sm-6 heading animateWrapper" data-animate-time="8">
			<hr class="first">
			<h1>Spark</h1>
			<hr class="last">
		</div>
		<div class="content animateWrapper" data-animate-time="6">
			<p>In this changing economy, your business needs a simple, fast solution to your web strategy that doesn't leave you pinching pennies for the next 3 years. On the other hand, you have a business to run! You don’t have time to keep track of platform updates, web technologies, social media trends, and marketing tactics. With Brushfire Managed Web Presence, your business can have a custom website that looks great across all platforms and devices for a fraction of the cost while allowing you to reap the benefits of a cohesive web strategy. Call today to get the conversation going or start your custom quote now!</p>
		</div>
</section>
@endsection

@section('footer')
	<script>
	var lastScrollTop = 0;
	$(window).scroll(function(event){
	   var st = $(this).scrollTop();
	   if (st > lastScrollTop){
	       $('.fa-angle-double-down').removeClass('flipped');
	   } else {
	      $('.fa-angle-double-down').addClass('flipped');
	   }
	   lastScrollTop = st;
	});
	</script>
@endsection