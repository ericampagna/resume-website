@extends('layouts.main')

@section('title', 'About Us')

@section('description', 'Find out where we have been and where we are going. Get to know the Brushfire Creative team.')

@section('keywords', 'Brushfire team, creative team, staff, brushfire history, about Brushfire Creative, Brushfire Creative office')

@section('bodyClass', 'about')

@section('content')
<section class="page-top">
	<h1 class="animated fadeInUp" style="color: #ffffff">Who is Eric Campagna?</h1>
</section>
<section class="page-content animateWrapper" data-animateTop="0">

<p class="about-copy">I am is a Full Stack Developer with 8 years of hands-on development experience building websites, web applications, platforms, Ecommerce and databases on a variety of environments. I am adept at understanding the requirements of the clients and delivering projects accordingly. I am capable of maintaining code by fixing bugs as well as conducting testing and I enjoy collaborating with a team to achieve deadlines. </p>

<p class="about-copy">But really I am more than that. I am also a husband and father of three, dog lover, and coffee snob. When I am not in front of my computer or with my family you can probably find me in the woods or by a lake, backpacking, camping, hiking or fishing. Our family lives in the country and enjoys the country life of growing our own food, raising chickens, keeping bees and living off the land God has given us. My local community means a lot to me and I serve on serveal commitees including the Planning Team for the Wabash County Farmer's Market and the Market Street MTC Board. Another passions of mine is music. I play guitar, drums and enjoy serving as the Music Director at Mount Carmel Community Church.  </p>

<img class="about-pic" src="/assets/images/family.jpg" width="25%">
</section>

@endsection