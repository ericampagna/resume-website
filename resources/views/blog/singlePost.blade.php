@extends('layouts.main')

@section('title', $post->title)

@section('description', $post->post_excerpt)

@section('ogImage', $post->thumbnail->attachment->url)
@section('ogUrl', 'https://brushfirecreative.com/blog/'.$post->slug)

@section('bodyClass', 'singlePost')

@section('content')
<section class="page-top ">
	<div class="page-top-wrapper">
		<h2 class="animated fadeInUp" style="color: #ffffff">{{$post->title}}</h2>
		<h5 class="Post-author"><i>{{date('m-d-Y', strtotime($post->post_date))}}</i> | {{$post->author->display_name}}</h5>
	</div>
	@if(isset($post->thumbnail->attachment))
	<img src="{{$post->thumbnail->attachment->url}}" title="{{$post->thumbnail->attachment->title}}" alt="{{$post->thumbnail->attachment->alt}}" >
	@endif
</section>
<section class="page-content Post animateWrapper" data-animateTop="0">
	<div class="Post-content">{!! nl2br($post->content) !!}</div>
	<!-- AddToAny BEGIN -->
	<div class="a2a_kit a2a_kit_size_32 SocialShare">
		<a class="a2a_button_facebook"></a>
		<a class="a2a_button_twitter"></a>
		<a class="a2a_button_linkedin"></a>
	</div>

	<script async src="https://static.addtoany.com/menu/page.js"></script>

	<!-- AddToAny END -->
</section>

<section class="AuthorPosts">
	@foreach($authorPosts as $authorPost)
		<a href="/blog/{{$authorPost->slug}}" class="AuthorPosts--post">
			<div class="AuthorPosts--post-title">
				<h5>{{$authorPost->title}}<br><hr></h5>

			</div>
			@if(isset($authorPost->thumbnail->attachment))
				<img src="{{$authorPost->thumbnail->attachment->url}}" title="{{$authorPost->thumbnail->attachment->title}}" alt="{{$authorPost->thumbnail->attachment->alt}}" >
			@endif
		</a>
	@endforeach
</section>
@endsection

@section('footer')
<script>
	var $sidebar   = $(".SocialShare"),
		$post	   = $('.Post');
        $window    = $(window),
        offset     = $sidebar.offset();

        console.log($window.scrollTop());
        console.log($post.scrollTop());
		$window.scroll(function() {
	        if ($window.scrollTop() >= $post.offset().top) {
	            $sidebar.addClass('scrolling');
	        }
	        else {
	            $sidebar.removeClass('scrolling');
            };
    	});
	</script>
@endsection
