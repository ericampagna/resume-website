@extends('layouts.main')

@section('title', 'Blog')

@section('description', 'Keep up-to-date with what is going on. Check out the Brushfire Creative blog.')

@section('keywords', 'Brushfire Creative, blog, posts, news, information, updates')

@section('bodyClass', 'blog')

@section('content')
<section class="page-top">
	<h1 class="animated fadeInUp" style="color: #ffffff">Blog<small></small></h1>
</section>
<section class="page-content BlogList" data-animateTop="0">

	@foreach($posts as $post)
		<div class="BlogList--post">
			<a href="/blog/{{$post->slug}}" class="box-link animateHover">
				<div class="BlogList--content-wrapper">
					<div class="BlogList--content">
						<h2>{{$post->title}}</h2>
						<hr>
						<p>{{$post->excerpt}}</p>
					</div>
				</div>
			</a>
			@if(isset($post->thumbnail->attachment))
				<img src="{{$post->thumbnail->attachment->url}}" title="{{$post->thumbnail->attachment->title}}" alt="{{$post->thumbnail->attachment->alt}}" >
			@endif
		</div>
	@endforeach

	{{ $posts->links() }}

</section>
@endsection