
@extends('layouts.main')

@section('title', 'B&amp;D Independence - Case Study')

@section('bodyClass', 'case-study')

@section('content')
<section class="page-top bd-portfolio-top">
    <h1 class="animated fadeInUp">B&amp;D Independence<small>A Global Brand</small></h1>
</section>
<section class="case-study bd-case">
    <div class="summary animateWrapper">
        <div class="offCanvasLeft case-content">
            <h4>Overview</h4>
            <p>B&amp;D is a global industry leader that needed a focused approach to keep at the forefront of the minds of dealers and consumers alike. A one-sided strategy was not going to fit this rapidly expanding company or their ever-changing market. All touch points needed to lead to brand recognition and sales.</p>
        </div>
        <div class="device iphone6 black offCanvasRight stickRightPhone ">
            <div class="top-bar"></div>
            <div class="sleep"></div>
            <div class="volume"></div>
            <div class="camera"></div>
            <div class="sensor"></div>
            <div class="speaker"></div>
            <div class="screen">
                <img class="deviceImage" src="/assets/images/bd-home-mobile.jpg" alt="B&D Independence homepage on Mobile device">
            </div>
            <div class="home"></div>
            <div class="bottom-bar"></div>
        </div>
    </div>
    <div class="animateWrapper case-quote" data-animateTop="0">
    <hr class="animate">
    <quote class="animate">"We market our products throughout the United States, Canada and Europe. I didn’t feel our website portrayed the professionalism that is required and expected of a company like ours."</quote>
    <hr class="animate">
    </div>
    <div class="challenge animateWrapper">
        <img class="offCanvasLeft Image" src="/assets/images/bd-bc.png" alt="B&D Independence Business Cards">
        <div class="offCanvasRight stickRight case-content pushDown">
            <h4>The Challenge</h4>
            <p>B&amp;D Independence came with a need to up their game globally. The company already had an established brand but no plan to take it to the next level with a coordinated marketing strategy. That strategy also needed to encompass both vehicle dealers and end users, a diverse group to reach.</p>
        </div>

    </div>
    <div class="animateWrapper case-quote" data-animateTop="0">
        <hr class="animate">
        <quote class="animate">"We discussed our needs with Eric and he immediately understood what we needed. Within a short period he presented us with some concepts that clearly conveyed our products and vision."</quote>
        <hr class="animate">
    </div>
    <div class="solution animateWrapper">
        <div class="device macbook fadeCanvas centerImage">
            <div class="top-bar"></div>
            <div class="camera"></div>
            <div class="screen">
                <img class="deviceImage" src="/assets/images/bd.gif" alt="B&D Independence website"/>
            </div>
            <div class="bottom-bar"></div>
        </div>
    </div>
    <div class="solution animateWrapper " style="padding-top:0;" data-animateTop="0">
        <div class="fadeCanvas case-content middle">
            <h4>The Solution</h4>
            <p>This was a company that needed several solutions on multiple platforms so we will hit the high points here. They needed new consumer websites for two for their products as well as a custom built portal to provide their dealers with a place to train new technicians and place orders for products.</p>
        </div>
    </div>
    <div class="animateWrapper case-quote" style="padding-top:0;" data-animateTop="0">
        <hr class="animate">
        <quote class="animate">"We have been with Eric for going on 5 years and I could not be more pleased with him. I’m constantly amazed that such talent is available in this area. He can do the same things that the big firms do at a fraction of the cost. Great product and great results!"</quote>
        <hr class="animate">
    </div>
     <div class="results animateWrapper" data-animateTop="0">
     <div class="device ipad silver fadeCanvas centerImage landscape">
                <div class="camera"></div>
                <div class="screen">
                     <iframe width="768" height="576" src="https://www.youtube.com/embed/4-Tv2MRABq8" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="home"></div>
            </div>
    </div>
    <div class="results animateWrapper" data-animateTop="0">
        <div class="fadeCanvas case-content">
            <h4>The Results</h4>
            <p>B&amp;D is a company that is constantly evolving and growing so their web presence must be flexible enough to do the same. Through print design, advertising, social media, web, and new product marketing, every message has its place.</p>
        </div>
        <div class="device macbook offCanvasRightDevice stickRightMacBook">
            <div class="top-bar"></div>
            <div class="camera"></div>
            <div class="screen">
                <img class="deviceImage" src="/assets/images/bd-portal.gif" alt="B&D Independence dealer training and ording portal"/>
            </div>
            <div class="bottom-bar"></div>
        </div>
    </div>
    </section>


@endsection