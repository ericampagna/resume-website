
@extends('layouts.main')

@section('title', 'Project Success - Case Study')

@section('bodyClass', 'case-study')

@section('content')
<section class="page-top ps-portfolio-top">
    <h1 class="animated fadeInUp">Project Success<small>Drastic Website Rebrand</small></h1>
    </section>
    <section class="case-study ps-case">
    <div class="summary animateWrapper">
        <div class="offCanvasLeft case-content">
            <h4>Overview</h4>
            <p>Project Success came to Brushfire for all-encompassing Marketing Consulting. They needed everything from an updated website to informational brochures to campaign messages that spanned social media, radio, newspaper and billboards. And it all had to flow together seamlessly.</p>
        </div>

        {{-- <img class="offCanvasRight stickRight" src="/assets/images/illusions-bridal-imac1.jpg"> --}}
        <div class="device macbook offCanvasRightDevice stickRightMacBook">
            <div class="top-bar"></div>
            <div class="camera"></div>
            <div class="screen">
                <img class="deviceImage" src="/assets/images/ps-desktop3.gif"/>
            </div>
            <div class="bottom-bar"></div>
        </div>

    </div>
    <div class="animateWrapper case-quote" data-animateTop="0">
    <hr class="animate">
    <quote class="animate">"We had no central hub, nothing to point people to for help on underage drinking and drug use."</quote>
    <hr class="animate">
    </div>
    <div class="challenge animateWrapper">
       <img class="offCanvasLeft Image" src="/assets/images/SocialLink_logo_final_color.png">
        <div class="offCanvasRight stickRight case-content">
            <h4>The Challenge</h4>
            <p>A totally useless website with no interaction, resources or real purpose. The goal of Project Success is to ***help reduce social access of alcohol in Wabash County*** Nothing about the old site was helping anyone find information on the topic.</p>
        </div>

    </div>
    <div class="animateWrapper case-quote" data-animateTop="0">
        <hr class="animate">
        <quote class="animate">"We loved the ideas and input Brushfire brought to the table. We had a huge say in the look and direction of the website. They really listened to our wants and needs and found ways to make them realities."</quote>
        <hr class="animate">
    </div>
    <div class="solution animateWrapper">
        {{-- <img class="fadeCanvas centerImage" src="/assets/images/illusions-bridal-phone.jpg"> --}}
        <div class="device iphone6 black fadeCanvas centerImage">
            <div class="top-bar"></div>
            <div class="sleep"></div>
            <div class="volume"></div>
            <div class="camera"></div>
            <div class="sensor"></div>
            <div class="speaker"></div>
            <div class="screen">
                <img class="deviceImage" src="/assets/images/ps-mobile.png">
            </div>
            <div class="home"></div>
            <div class="bottom-bar"></div>
        </div>
    </div>
<!--     <div class="solution animateWrapper">
        <img class="fadeCanvas centerImage seventy" src="/assets/images/PSmockupDesktop.jpg">
    </div> -->
    <div class="solution animateWrapper " style="padding-top:0;" data-animateTop="0">
        <div class="fadeCanvas case-content middle text-center">
            <h4>The Solution</h4>
            <p>First, a revamp of the logo. The thought behind the design was great! It just needed to be brought up to speed to fit the modern-looking website it was about to adorn. Second, an updated site worthy of the cause it supports. Knowing what it should contain and look like, however, was going to take some research. What did parents of teens care about? What did teens care about? Working closely with our client, we found answers by conducting focus groups of parents of teens and teens themselves. The responses we got were both surprising and affirming. Something had to be done to push information out to those that need it the most.</p><br />
<p>The site was really going to serve two very different audiences. Our solution was to give each group their own section of the website devoted to the content they cared about. For parents, we developed SocialLink – a place for parents to get reliable information on topics they were concerned about including alcohol, tobacco, marijuana, and health and safety issues. For teens, we wanted to highlight what their peers were saying about these topics. YAB, or Youth Advisory Board, is a group of students who try to model the safe and healthy lifestyle promoted by Project Success. The YAB section of the site is a place for board members to post blogs and see the current campaigns of Project Success.</p><br />

<p>Overall, the strategy was to organize resources and information in a way that was easy for parents and teens to find. Education and conversation was the ultimate goal here.</p><br />
        </div>
    </div>
    <div class="animateWrapper case-quote" style="padding-top:0;" data-animateTop="0">
        <hr class="animate">
        <quote class="animate">"We love our site. We get compliments on it by other coalitions regularly. It has proven to be an amazing campaign too for our coalition and source of reliable information for our community."</quote>
        <hr class="animate">
    </div>
     <div class="results animateWrapper" data-animateTop="0">
            <div class="device ipad silver fadeCanvas centerImage landscape">
                <div class="camera"></div>
                <div class="screen">
                      <img class="deviceImage" src="/assets/images/yab.png">
                </div>
                <div class="home"></div>
            </div>
    </div>
    <div class="results animateWrapper" data-animateTop="0">
        <div class="fadeCanvas case-results">
            <h4>The Results</h4>
            <p>Colorful, simple, easy to navigate. The entire site is fun and lively featuring fantastic photography and bold colors. Both audiences are easily able to find their way to the information they need as well as blogs and stories they didn’t think they were looking for. Overall, a huge improvement from where Project Success started.</p>
        </div>
         <!-- <img class="offCanvasRight rightCenter" src="/assets/images/PSmockupMacBook.jpg"> -->
    </div>
    </section>


@endsection