
@extends('layouts.main')

@section('title', 'Illusions Bridal - Case Study')

@section('bodyClass', 'case-study')

@section('content')
<section class="page-top">
    <h1 class="animated fadeInUp">Illusions Bridal<small>From Drab to Fab</small></h1>
    </section>
    <section class="case-study ">
    <div class="summary animateWrapper">
        <div class="offCanvasLeft case-content">
            <h4>Overview</h4>
            <p>A total transformation is what we like to see at Brushfire and that was certainly the case with Illusions Bridal! The results have made the client so happy to show off to her customers and vendors and we couldn't be more proud. Definitely one of our favorite re-branding stories yet!</p>
        </div>
        {{-- <img class="offCanvasRight stickRight1" src="/assets/images/illusions-bridal-imac1.jpg"> --}}
        <div class="device macbook offCanvasRightDevice stickRightMacBook">
            <div class="top-bar"></div>
            <div class="camera"></div>
            <div class="screen">
                <img class="deviceImage" src="/assets/images/illusions.gif"/>
            </div>
            <div class="bottom-bar"></div>
        </div>
    </div>
    <div class="animateWrapper case-quote" data-animateTop="0">
    <hr class="animate">
    <quote class="animate">"Our old site was just that – old and out of date. Nothing about is was young, fun, or exciting like my clientele. I needed something that drew them in. I needed a brand that caught their attention."</quote>
    <hr class="animate">
    </div>
    <div class="challenge animateWrapper">
        <img class="offCanvasLeft Image" src="/assets/images/illusions-bridal-bc1.jpg">
        <div class="offCanvasRight stickRight case-content pushDown">
            <h4>The Challenge</h4>
            <p>Illusions Bridal &amp; Tuxedo came to us as Illusions Bridal &amp; Party Shop. A change of identity was needed because their customers weren’t thinking of them for all the other services they offered. The Brushfire team went to work on a total brand overhaul that would keep Illusions in the forefront of customer’s minds when it came to all things wedding and special occasion.</p>
        </div>

    </div>
    <div class="animateWrapper case-quote" data-animateTop="0">
        <hr class="animate">
        <quote class="animate">"Working with Brushfire was easy even being separated by a time zone. They were always available to visit me at the shop for my convenience. I really felt like they understood what I wanted for my shop overall."</quote>
        <hr class="animate">
    </div>
    <div class="solution animateWrapper">
        {{-- <img class="fadeCanvas centerImage" src="/assets/images/illusions-bridal-phone.jpg"> --}}
        <div class="device iphone6 black fadeCanvas centerImage">
            <div class="top-bar"></div>
            <div class="sleep"></div>
            <div class="volume"></div>
            <div class="camera"></div>
            <div class="sensor"></div>
            <div class="speaker"></div>
            <div class="screen">
                <img class="deviceImage" src="/assets/images/illusions-tux-mobile.jpg">
            </div>
            <div class="home"></div>
            <div class="bottom-bar"></div>
        </div>
    </div>
    <div class="solution animateWrapper " style="padding-top:0;" data-animateTop="0">
        <div class="fadeCanvas case-content middle ">
            <h4>The Solution</h4>
            <p>First, the new brand. The client needed something elegant and simple, something that would stand out in the noise of the market. After a logo was selected, the site started to take shape. Echoing the simplicity of the new brand, Illusions’ site was going to allow their products to speak for themselves. Beautiful photography and video production (by Eyenamics) was key to the success of the site’s first impression. The Brushfire team also suggested a sub-brand to help promote the Prom &amp; Tux division of the shop. This would allow customers to navigate directly to the portion of the site they are looking for.</p>
        </div>
    </div>
    <div class="animateWrapper case-quote" style="padding-top:0;" data-animateTop="0">
        <hr class="animate">
        <quote class="animate">"I'm beyond thrilled with the way the whole new brand image has turned out. I wasn’t fully expecting to change the logo but I knew I had to have it when Brushfire did new mockups for me. When they presented the final product of the website I had almost no changes to make! I get compliments from clients and vendors both local and national on how beautiful the site is. That makes me feel great about the whole project!"</quote>
        <hr class="animate">
    </div>
     <div class="results animateWrapper" data-animateTop="0">
            <img class="fadeCanvas centerImage fullImage Image" src="/assets/images/illusions-bridal-brochure.jpg">
    </div>
    <div class="results animateWrapper" data-animateTop="0">
        <div class="fadeCanvas case-content">
            <h4>The Results</h4>
            <p>Stunning and simple with room to breathe, the entire brand has now been modernized and given a real identity. The bold yet graceful icon draws your eye to the new logo and is recognizable throughout all the new branding. Everything from new styles to store hours to the rental price of tablecloths is easy to find. There is nothing old and outdated about the new Illusions Bridal!</p>
        </div>
         {{-- <img class="fadeCanvas stickRight1" src="/assets/images/illusions-bridal-phone.jpg"> --}}
          <div class="device iphone6 gold fadeCanvas stickRightPhone rotated">
            <div class="top-bar"></div>
            <div class="sleep"></div>
            <div class="volume"></div>
            <div class="camera"></div>
            <div class="sensor"></div>
            <div class="speaker"></div>
            <div class="screen">
                <img class="deviceImage" src="/assets/images/illusions-prom-mobile.jpg">
            </div>
            <div class="home"></div>
            <div class="bottom-bar"></div>
        </div>
    </div>
    </section>


@endsection