<html>
<head>
	<title>@yield('title') - Eric Campagna</title>
	<!-- Meta Info -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-title" content="Eric Campagna">
    <meta name="application-name" content="Eric Campagna">
    <meta name="description" content="@yield('meta_desc')">
    <meta name="keywords" content="@yield('meta_keywords')">
    <meta property="fb:app_id" content="418521371820617">
    <meta property="og:url" content="@yield('ogUrl', 'http://ericcampagna.com/')" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="@yield('title')" />
    <meta property="og:description" content="@yield('description')" />
    <meta property="og:image" content="@yield('ogImage', '/favicon.png')" />
    <!-- Favicon / App Icon settings -->
	<link rel="icon" type="image/png" href="/favicon.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#002F3B">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#002F3B">
	<!--  -->
	<link rel="stylesheet" href="/assets/css/reset.css"/>
	<link rel="stylesheet" href="/assets/css/style.css"/>
	<script src="https://use.typekit.net/css6woe.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<script src="https://use.fontawesome.com/44771330c7.js"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135373786-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-135373786-1');
</script>

</head>

<body class="@yield('bodyClass')-page" id="bc-app">
	<!-- Hidding Menu -->
	<aside class="menu">
		<ul class="top-menu">
			<li class="services"><a href="/assets/Eric-Campagna_resume.pdf"><span>Resume</span></a></li>
			<li class="work"><a href="/"><span>My Work</span><div class="zoom-box"></div></a></li>
			<li class="about"><a href="/about"><span>About</span><div class="zoom-box"></div></a></li>
			<li class="start"><a href="/start-a-project"><span>Start a Project</span><div class="zoom-box"></div></a></li>
			{{-- <li class="spark"><a href="/spark"><span>Spark</span><div class="zoom-box"></div></a></li> --}}
			{{-- <li class="team"><a href="/team"><span>Team</span><div class="zoom-box"></div></a></li> --}}
			{{-- <li class="blog"><a href="/blog"><span>Blog</span><div class="zoom-box"></div></a></li> --}}
{{-- 			<li class="contact"><a href="/contact"><span>Contact</span><div class="zoom-box"></div></a></li> --}}
		</ul>
	</aside>
	<header class="top-bar">
		{{-- <h6>Eric Campagna - Web Developer</h6> --}}
		<!-- Menu Button -->
		<div class="menu-btn">
			<span>Menu</span>
			<div class="menu-icon">
				<span class="top"></span>
				<span class="middle"></span>
				<span class="bottom"></span>
			</div>
		</div>
	</header>
	@yield('content')
	<footer>
		<div id="inner-footer" class="container animated fadeInUp">
					<a class="footer-contact" href="/start-a-project"> Hire Me <i class="fa fa-long-arrow-right move"></i></a>
					<div class="social-icons">
{{-- 						<a href="https://facebook.com/brushfirecreative" target="_blank "><span class="fa fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span></a>
						<a href="https://twitter.com/brushfirecmm" target="_blank "><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x fa-inverse"></i></span></a> --}}
						<a href="https://www.linkedin.com/in/eric-campagna" target="_blank "><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-linkedin fa-stack-1x fa-inverse"></i></span></a>
						<a href="https://instagram.com/ericampagna/" target="_blank "><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-instagram fa-stack-1x fa-inverse"></i></span></a>
						<a href="https://github.com/ericampagna" target="_blank "><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-github fa-stack-1x fa-inverse"></i></span></a>
					</div>
					<p class="source-org copyright">&copy;<?php echo date('Y'); ?> Eric Campagna</p>
				</div>
	</footer>
	<script src="https://unpkg.com/vue@2.1.8/dist/vue.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="/assets/js/min/jquery.mousewheel.min.js"></script>
	<script src="/assets/js/min/hoverIntent.min.js"></script>
	<script src="/assets/js/min/ScrollMagic.min.js"></script>
	{{-- App S --}}
	<script src="/assets/js/scripts.js"></script>
	<script src="/js/main.js"></script>
	@yield('footer')
</body>
</html>