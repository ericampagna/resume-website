@extends('layouts.main')

@section('title', 'Contact')

@section('description', 'Have an idea for a new project? Need help with an exsiting project? Just want to say hi? We are here!')

@section('keywords', 'Project, Contact for a Website, Marketing Services, Website Project, Business Project')

@section('bodyClass', 'contact')

@section('content')

<section class="page-top">
	<h1 class="animated fadeInUp" style="color: #ffffff">Contact Me<small>Have an idea for a new project? Need help with an exsiting project? Just want to say hi? </small></h1>
	</section>
  <div id="map"></div>

<div id="contact-form">
        <form class="contact-form" role="form" method="POST" action="{{ url('/contact') }}">
         <input id="name" type="text" class="input-full" name="name" value="{{ old('name') }}" placeholder="Name">
         @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
        <input id="email" type="email" class="input-full" name="email" value="{{ old('email') }}" placeholder="E-Mail">

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif

        <input id="phone" type="text" class="input-full" name="phone" value="{{ old('name') }}" placeholder="Phone">
         @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif

        <textarea name="message" cols="50" rows="10" placeholder="How can we help you?"></textarea>
        @if ($errors->has('message'))
            <span class="help-block">
                <strong>{{ $errors->first('message') }}</strong>
            </span>
        @endif

        <div>
            <button type="submit" class="btn submit-button">
                Send
            </button>
        </div>
    </form>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrG0Y0SuA9nSs8o_gp1TFevTTFI1Yo_3o&callback=initMap"
    async defer></script>
    <script src="/assets/js/map.js"></script>
	<script>



 


</script>

@endsection