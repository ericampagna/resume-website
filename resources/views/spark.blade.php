@extends('layouts.main')

@section('title', 'Spark Managed Web Presence')

@section('description', 'Managed Web Presence. The internet is a big place. Let us manage it for you.')

@section('keywords',  'Managed Web Presense, web management, web strategy, web technologies, social media trends, Web Development, Web Development platform, responsive design, copy proofing, custom design, location services, website updates, SEO, Web Analytics Reports, Uptime monitoring, Security and backups, web security, hosting, affordable website, small business website' )

@section('bodyClass', 'spark')

@section('content')
<section class="page-top ">
	<h1 class="animated fadeInUp" style="color: #ffffff">Spark<small>
	Managed Web Presence. <br>The internet is a big place. Let us manage it for you.</small></h1>
</section>
<section class="spark-intro ">
	<div class="spark-intro-text ">
		<p>
			In this changing economy, your business needs a simple, fast solution to your web strategy that doesn't leave you pinching pennies for the next 3 years. On the other hand, you have a business to run! You don’t have time to keep track of platform updates, web technologies, social media trends, and marketing tactics. With Brushfire Managed Web Presence, your business can have a custom website that looks great across all platforms and devices for a fraction of the cost while allowing you to reap the benefits of a cohesive web strategy. Call today to get the conversation going or start your custom quote now!
		</p>
	</div>
	<div class="spark-intro-call">
		<h2>Get Started Today! </h2>
		<h3><a href="tel:6182634547">618-263-4547</a></h3>
		{{-- <a href="/managed-web-presence/custom-quote" class="blue-btn">Start a Custom Quote <i class="fa fa-arrow-right"></i></a>--}}
	</div>
</section>
<section class="callout animateWrapper" data-animate-time="8">
	<h2 class="fadeCanvas">Starting at only<br>$500 for setup &amp; $225 a month!</h2>
</section>
<section class="Blocks">
	<h4 class="text-center">What do you get?</h4>
	<div class="Block design">
		<span class="fa fa-paint-brush fa-2x"></span>
		<h6>Custom Design</h6>
		<p>Nobody wants a site that look like everyone elses.<p>
	</div>
	<div class="Block design">
		<span class="fa fa-desktop fa-2x"></span>
		<h6>Responsive Design</h6>
		<p>Your site should look great on all devices, all sizes, all browsers!</p>
	</div>
	<div class="Block setup">
		<span class="fa fa-pencil-square fa-2x"></span>
		<h6>Copy Proofing</h6>
		<p>We make sure you sound like you.</p>
	</div>
	<div class="Block setup">
		<span class="fa fa-map-signs fa-2x"></span>
		<h6>Location Services</h6>
		<p>We'll put you on the map, literally!</p>
	</div>
	<div class="Block setup">
		<span class="fa fa-repeat fa-2x"></span>
		<h6>Updates</h6>
		<p>Your site can’t help you if the information is out-of-date. It could even hurt you.</p>
	</div>
	<div class="Block marketing">
		<span class="fa fa-search-plus fa-2x"></span>
		<h6>SEO</h6>
		<p>We'll let Google know you're here.</p>
	</div>
	<div class="Block marketing">
		<span class="fa fa-area-chart fa-2x"></span>
		<h6>Web Analytics</h6>
		<p>Do you know who is visiting your site? We do.</p>
	</div>
	<div class="Block security">
		<span class="fa fa-gears fa-2x"></span>
		<h6>Uptime Monitoring</h6>
		<p>Your site has zero value if it’s “down”.</p>
	</div>
	<div class="Block security">
		<span class="fa fa-shield fa-2x"></span>
		<h6>Security &amp; Backups</h6>
		<p>We play offense not defense.</p>
	</div>
	<div class="Block security">
		<span class="fa fa-server fa-2x"></span>
		<h6>Hosting</h6>
		<p>Like a house for your website, but with a moat and laser sharks.</p>
	</div>
	<div class="Block security">
		<span class="fa fa-life-ring fa-2x"></span>
		<h6>Support</h6>
		<p>Our goal is your success. Let us help you!</p>
	</div>
	<div class="Block photo">
		<span class="fa fa-cubes fa-2x"></span>
		<h6>Extensions</h6>
		<p>Need more? No problem, we've got an extension for that.</p>
		<i class="fa fa-arrow-down"></i>
	</div>
</section>
<section class="callout animateWrapper" data-animate-time="8">
	<h2 class="fadeCanvas">Extentions &agrave; la carte</h2>
	<i>take your site to the next level</i>
</section>
<section class="Blocks">
<div class="Block setup">
		<span class="fa fa-file-code-o fa-2x"></span>
		<h6>Extra Pages</h6>
		<p>Sometimes you need more than 5 pages, we understand.</p>
		<i>$75 per page</i>
	</div>
	<div class="Block setup">
		<span class="fa fa-keyboard-o fa-2x"></span>
		<h6>Copywriting</h6>
		<p>Don't have the words? We do!</p>
		<i>$50 per page</i>
	</div>
	<div class="Block setup">
		<span class="fa fa-table fa-2x"></span>
		<h6>Custom Forms</h6>
		<p>Need users to send you info? Custom forms are the answer!</p>
		<i>Quoted based on need</i>
	</div>
	<div class="Block design">
		<span class="fa fa-lightbulb-o fa-2x"></span>
		<h6>Logo Design</h6>
		<p>Show them who you are.</p>
		<i>$500</i>
	</div>
	<div class="Block design">
		<span class="fa fa-file-image-o fa-2x"></span>
		<h6>Logo to Digital</h6>
		<p>Only have your logo as a PDF Scan? No Problem!</p>
		<i>$200</i>
	</div>
	<div class="Block photo">
		<span class="fa fa-user fa-2x"></span>
		<h6>Head Shot</h6>
		<p>Let's see that pretty face.</p>
		<i>$179 each</i>
		</div>
	<div class="Block photo">
		<span class="fa fa-photo fa-2x"></span>
		<h6>Stock Photography</h6>
		<p>Don't have the right photos? We've got you covered!</p>
		<i>$25 an image</i>
	</div>
	<div class="Block photo">
		<span class="fa fa-camera-retro fa-2x"></span>
		<h6>On-site Photography</h6>
		<p>Have a beautiful building or people? We'll come to you!</p>
		<i>Quoted based on need</i>
	</div>
	<div class="Block marketing">
		<span class="fa fa-bullhorn fa-2x"></span>
		<h6>Social Media</h6>
		<p>No time to mess with Facebook posts, retweets, or Instagram feeds? </p>
		<i>$200 per month</i>
	</div>
	<div class="Block marketing">
		<span class="fa fa-bullseye fa-2x"></span>
		<h6>Search Engine Marketing</h6>
		<p>Maximizing views on your site increases value.</p>
		<i>$300 per month</i>
	</div>
	<div class="Block ecom x2">
		<span class="fa fa-shopping-cart fa-2x"></span>
		<h6>E-Commerce</h6>
		<p>Coming Soon!</p>
	</div>
</section>
<section class="examples">
	<h2>Who uses our Managed Web Presence?</h2>
		<div class="spark-example">
			<p>Wabash Valley Youth in Action</p>
			<div class="faux-browser">
				<div class='frame'><span></span><span></span><span></span></div>
				<img src="/assets/images/screencapture-wvyouthinaction.jpg">
			</div>
		</div>
		<div class="spark-example">
			<p>Casa di Campagna Farm</p>
			<div class="faux-browser">
				<div class='frame'><span></span><span></span><span></span></div>
				<img src="/assets/images/screencapture-casadicampagna-farm-1472736947611.jpg">
			</div>
		</div>
		<div class="spark-example">
			<p>Mt. Carmel Community Church</p>
			<div class="faux-browser">
				<div class='frame'><span></span><span></span><span></span></div>
				<img src="/assets/images/screencapture-mtccommunity-church-1472736567960.jpg">
			</div>
		</div>
</section>
@endsection