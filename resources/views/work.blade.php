@extends('layouts.main')

@section('title', 'Our Portfolio')

@section('description', 'Here at Brushfire, we take pride in the work we do for clients. But do not just take our word for it. See for your self.')

@section('keywords','work, clients, portfolio, design work, designs, digital clients, website clients, branding clients, marketing, marketing clients, Brushfire work, print design, web design, business solutions, business intelligence, business portal, business dashboard')

@section('bodyClass', 'work')

@section('content')
<section class="page-top">
	<h1 class="animated fadeInUp" style="color: #ffffff">Our Work<small>We take pride in what we do. But don't take our word for it. See for yourself. </small></h1>
</section>
<section class="box-grid">
<!-- 	<div class="box animated fadeInLeft">
	<img src="/assets/images/kct_building.jpg" />
		<a class="box-link box-slide-down" href="">
			<div class="zoom-box"></div>
			<div class="box-content">
				<h2>Knox County Transparency Portal</h2>
				<hr>
				<p>digital</p>
			</div>
		</a>
	</div> -->
<!-- 	<div class="box animated fadeInRight">
		<img src="/assets/images/teller.jpg" />
		<a class="box-link box-slide-down" href="">
			<div class="zoom-box"></div>
			<div class="box-content">
				<h2>First National Bank</h2>
				<hr>
				<p>strategy/branding/social</p>
			</div>
		</a>
	</div> -->
	<div class="box animated fadeInLeft">
	<img src="/assets/images/bd.jpg" />
		<a class="box-link box-slide-down" href="work/bd-independence">
			<div class="zoom-box"></div>
			<div class="box-content">
				<h2>B&amp;D Independence</h2>
				<hr>
				<p>strategy/branding/digital/social</p>
			</div>
		</a>
	</div>
<!-- 	<div class="box animated fadeInRight">
	<img src="/assets/images/loudermilk.jpg" />
		<a class="box-link box-slide-down" href="">
			<div class="zoom-box"></div>
			<div class="box-content">
				<h2>Loudermillk Contracting</h2>
				<hr>
				<p>digital</p>
			</div>
		</a>
	</div> -->
<!-- 	<div class="box animated fadeInLeft">
	<img src="/assets/images/wgh.jpg" />
		<a class="box-link box-slide-down" href="">
			<div class="zoom-box"></div>
			<div class="box-content">
				<h2>Wabash General Hospital</h2>
				<hr>
				<p>strategy/branding/digital</p>
			</div>
		</a>
	</div> -->
	<div class="box animated fadeInRight">
	<img src="/assets/images/illusions-case.png" />
		<a class="box-link box-slide-down" href="/work/illusions-bridal">
			<div class="zoom-box"></div>
			<div class="box-content slide animated hidden">
				<h2>Illusions Bridal</h2>
				<hr>
				<p>strategy/branding/digital</p>
			</div>
		</a>
	</div>
	<div class="box animated fadeInLeft">
	<img src="/assets/images/project-success.jpg" />
		<a class="box-link box-slide-down" href="/work/project-success">
			<div class="zoom-box"></div>
			<div class="box-content">
				<h2>Project Success</h2>
				<hr>
				<p>strategy/branding/digital/social</p>
			</div>
		</a>
	</div>
	<div class="box animated fadeInRight" id="">
	<img src="/assets/images/pocketbarn.jpg" />
		<a class="box-link box-slide-down" href="/work/pocketbarn" v-on:click.prevent.stop="linkStop" >
			<div class="zoom-box"></div>
			<div class="box-content">
				<h2>Pocketbarn</h2>
				<hr>
				<p>strategy/branding/digital</p>
			</div>
		</a>
	</div>
</section>
<section class="clients">
	<h4 style="text-align: center;">OUR CLIENTS</h4>
<a href="http://wabashgeneral.com" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-2010 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/WGH_75pxH.png" alt="WGH_75pxH" width="181" height="75"></div></a><a href="http://bdindependence.com" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1966 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/BDIndepen_75pxH.png" alt="BDIndepen_75pxH" width="128" height="75"></div></a> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-2006" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/Vigo_75pxH.png" alt="Vigo_75pxH" width="242" height="75"></div> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-medium wp-image-2008" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/Watler_75pxH-300x71.png" alt="Watler_75pxH" width="300" height="71" srcset="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/Watler_75pxH-300x71.png 300w, https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/Watler_75pxH.png 315w" sizes="(max-width: 300px) 100vw, 300px"></div> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-2007" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/VintageCollections_75pxH.png" alt="VintageCollections_75pxH" width="150" height="75"></div> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-2004" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/TheGrind_75pxH.png" alt="TheGrind_75pxH" width="207" height="75"></div> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-2005" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/Velox_75pxH.png" alt="Velox_75pxH" width="235" height="75"></div> <a href="http://swiftsanctuary.rentals" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-2003 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/SwiftSanctuary_75pxH.png" alt="SwiftSanctuary_75pxH" width="172" height="75"></div></a> <a href="http://spiritofvincennes.org" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-2002 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/SpiritofVinn_75pxH.png" alt="SpiritofVinn_75pxH" width="223" height="75"></div></a> <a href="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/SharpWilliams_75pxH.png"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-2001" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/SharpWilliams_75pxH.png" alt="SharpWilliams_75pxH" width="204" height="75"></div></a> <a href="http://illusionsbridal.com" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1985 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/IllusionsBridal_75pxH.png" alt="IllusionsBridal_75pxH" width="206" height="75"></div></a> <a href="http://wvyouthinaction.com" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-2012 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/YIA_75pxH.png" alt="YIA_75pxH" width="72" height="75"></div></a> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-2011" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/WVC_75pxH.png" alt="WVC_75pxH" width="92" height="75"></div> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-2000" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/Rz_75pxH.png" alt="Rz_75pxH" width="85" height="75"></div> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-2009" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/Weekare_75pxH.png" alt="Weekare_75pxH" width="216" height="75"></div> <a href="http://ribberfest.com"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1999 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/Ribberfest_75pxH.png" alt="Ribberfest_75pxH" width="225" height="75"></div></a> <a href="http://wcprojectsuccess.com" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1997 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/ProjectSuccess_75pxH.png" alt="ProjectSuccess_75pxH" width="163" height="75"></div></a> <a href="http://wabashcountychamber.com" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1998 size-medium" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/RetailMerchants_75pxH-300x60.png" alt="RetailMerchants_75pxH" width="300" height="60" srcset="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/RetailMerchants_75pxH-300x60.png 300w, https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/RetailMerchants_75pxH.png 375w" sizes="(max-width: 300px) 100vw, 300px"></div></a><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1995" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/NantucketProperties_75pxH.png" alt="NantucketProperties_75pxH" width="237" height="75"></div> <a href="http://producepatchfarmmarket.com"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1996 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/ProducePatch_75pxH.png" alt="ProducePatch_75pxH" width="96" height="75"></div></a> <a href="http://mundyrealestate.com/" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1994 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/Mundy_75pxH.png" alt="Mundy_75pxH" width="235" height="75"></div></a><a href="http://maplehillrestorations.com" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1993 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/MHR_75pxH.png" alt="MHR_75pxH" width="198" height="75"></div></a> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1990" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/Keepes_75pxH.png" alt="Keepes_75pxH" width="150" height="75"></div> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-medium wp-image-1991" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/KiefferBros_75pxH-300x42.png" alt="KiefferBros_75pxH" width="300" height="42" srcset="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/KiefferBros_75pxH-300x42.png 300w, https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/KiefferBros_75pxH.png 536w" sizes="(max-width: 300px) 100vw, 300px"></div> <a href="http://mcleanimp.com" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1992 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/McLean_75pxH.png" alt="McLean_75pxH" width="188" height="75"></div></a> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1988" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/JAACO_75pxH.png" alt="JAACO_75pxH" width="192" height="75"></div><a href="http://kcpl.lib.in.us/" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1989 size-medium" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/kcpl_75pxH-300x71.png" alt="kcpl_75pxH" width="300" height="71" srcset="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/kcpl_75pxH-300x71.png 300w, https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/kcpl_75pxH.png 319w" sizes="(max-width: 300px) 100vw, 300px"></div></a> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1979" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/GCT_75pxH.png" alt="GCT_75pxH" width="125" height="75"></div> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1986" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/IllusionsProm_75pxH.png" alt="IllusionsProm_75pxH" width="174" height="75"></div> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1987" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/IMP_75pxH.png" alt="IMP_75pxH" width="236" height="75"></div> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1984" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/HFI_75pxH.png" alt="HFI_75pxH" width="115" height="75"></div><a href="http://harrisonstorage.com" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1983 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/harrisonstorage_75pxH.png" alt="harrisonstorage_75pxH" width="139" height="75"></div></a> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1982" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/Halbig_75pxH.png" alt="Halbig_75pxH" width="293" height="75"></div><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1981" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/GrowSmellTaste_75pxH.png" alt="GrowSmellTaste_75pxH" width="273" height="75"></div><a href="http://www.goshenfirstumc.org/" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1980 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/GFUMC_75pxH.png" alt="GFUMC_75pxH" width="164" height="75"></div></a> <a href="http://www.friendsofgoosepond.org/" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1977 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/FoGP_75pxH.png" alt="FoGP_75pxH" width="120" height="75"></div></a> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1978" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/FSR_75pxH.png" alt="FSR_75pxH" width="102" height="75"></div> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-medium wp-image-1976" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/FNB_75pxH-300x50.png" alt="FNB_75pxH" width="300" height="50" srcset="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/FNB_75pxH-300x50.png 300w, https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/FNB_75pxH.png 449w" sizes="(max-width: 300px) 100vw, 300px"></div> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1975" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/FearheileyLumber_75pxH.png" alt="FearheileyLumber_75pxH" width="182" height="75"></div> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-medium wp-image-1972" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/EdwardJones_75pxH-300x55.png" alt="EdwardJones_75pxH" width="300" height="55" srcset="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/EdwardJones_75pxH-300x55.png 300w, https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/EdwardJones_75pxH.png 408w" sizes="(max-width: 300px) 100vw, 300px"></div><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1973" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/Fantastic4th_75pxH.png" alt="Fantastic4th_75pxH" width="109" height="75"></div> <a href="http://fbcvin.com" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1974 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/FBCVin_75pxH.png" alt="FBCVin_75pxH" width="133" height="75"></div></a> <a href="http://wabashcountychamber.com"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1970 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/Chamber_75pxH.png" alt="Chamber_75pxH" width="213" height="75"></div></a> <a href="http://deecane.com" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1971 size-medium" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/DeeCane_75pxH-300x65.png" alt="DeeCane_75pxH" width="300" height="65" srcset="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/DeeCane_75pxH-300x65.png 300w, https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/DeeCane_75pxH.png 346w" sizes="(max-width: 300px) 100vw, 300px"></div></a> <a href="http://mtccommunity.church/" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1969 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/CCOG_75pxH.png" alt="CCOG_75pxH" width="283" height="75"></div></a> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1968" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/CCC_75pxH.png" alt="CCC_75pxH" width="202" height="75"></div> <a href="http://blannberries.com" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1967 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/BlannBerries_75pxH.png" alt="BlannBerries_75pxH" width="114" height="75"></div></a> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1965" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/arcNinety_75pxH.png" alt="arcNinety_75pxH" width="196" height="75"></div><a href="http://mcaea.com/" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1963 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/AEA_75pxH.png" alt="AEA_75pxH" width="205" height="75"></div></a> <div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft size-full wp-image-1964" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/AndrewsOil_75pxH.png" alt="AndrewsOil_75pxH" width="150" height="75"></div> <a href="http://albiondentalclinic.com" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1961 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/ADC_75pxH.png" alt="ADC_75pxH" width="281" height="75"></div></a> <a href="http://adg-design.net" target="_blank"><div class="client_logo makeSquare" style="height: 78px;"><img class="alignleft wp-image-1962 size-full" src="https://s3.amazonaws.com/brushfire/wp-content/uploads/2015/09/ADG_75pxH.png" alt="ADG_75pxH" width="290" height="75"></div></a>
</section>
@endsection