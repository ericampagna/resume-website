var Vue = require('vue');
//var Resource = require('vue-resource');

//Components


//Vue.use(Resource);
//Submit the csrf token with every AJAX request
//Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');


//Directive send form though ajax instead of page refresh
Vue.directive('ajax', {
	//All paramaters this directive
	params: ['complete'],

	bind() {
		//Listen for the Form to be submitted,then pass along to the onFormSubission function
		this.el.addEventListener('submit', this.onFormSubmission.bind(this));
	},
	update() {

	},
	onFormSubmission(e) {
		 //Stop Submiting form
		e.preventDefault();
		//Get the request type of the form, ie. GET, POST, DELETE... also get the form action
		this.vm
			.$http[this.getRequestType()](this.el.action)
			.then(this.onComplete.bind(this))
			.catch(this.onError.bind(this))
		;
	},
	onComplete() {
			if (this.params.complete) {
				alert(this.params.complete)
			}
	},

	onError(response) {
		console.log(response);
	},

	getRequestType() {
		//Get submit method type from method input
		var method = this.el.querySelector('input[name="_method"]');
		//Check method to see id mehtod input exsited, if not get method from the form
		return (method ? method.value : this.el.method).toLowerCase();
	},

});


new Vue({
	el: '#bc-app',
	methods: {
		linkStop: function(event)
		{
			event.stopImmediatePropagation();;
		}
	}

});

$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[id=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top - 20
        }, 750,);
        return false;
      }
    }
  });
});


