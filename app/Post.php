<?php

namespace App;

use Corcel\Post as Corcel;

class Post extends Corcel
{
    protected $connections = 'wordpress';
    protected $with = 'attachment';

    public function author(){
    	return $this->hasOne('App\User', 'post_author', 'id');
    }


}
