<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Post;

class BlogController extends Controller
{
    public function index()
    {
    	$posts = Post::type('post')->with('attachment')->status('publish')->orderBy('id', 'desc')->paginate(15);

    	return view('blog.posts')->with(['posts' => $posts]);
    }

    public function show($slug)
    {
    	$post = Post::slug($slug)->first();
    	$author = $post->author;
    	$authorPosts = $author->posts()->status('publish')->type('post')->orderBy('id', 'desc')->get();

    	$authorPosts = $authorPosts->filter(function ($value) use ($post) {
    		return $value->ID != $post->ID;
		})->take(3);


    	return view('blog.singlePost')->with(['post' => $post, 'authorPosts' => $authorPosts]);
    }
}
